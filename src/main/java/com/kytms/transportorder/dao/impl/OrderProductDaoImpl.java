package com.kytms.transportorder.dao.impl;

import com.kytms.core.dao.impl.BaseDaoImpl;
import com.kytms.core.entity.OrderProduct;
import com.kytms.transportorder.dao.OrderProductDao;
import org.springframework.stereotype.Repository;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * @author 臧英明
 * @create 2018-01-05
 */
@Repository(value = "OrderProductDao")
public class OrderProductDaoImpl extends BaseDaoImpl<OrderProduct> implements OrderProductDao<OrderProduct> {
}
